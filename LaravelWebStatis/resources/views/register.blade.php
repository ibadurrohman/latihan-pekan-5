<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
        
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="POST">
            @csrf
            <label>First name :</label><br><br>
            <input type="text" name="name"><br><br>
            <label>Last name :</label><br><br>
            <input type="text" name="lastname"><br><br>
            
            <label>Gender</label><br><br>
            <input type="radio" name="gender">Male<br>
            <input type="radio" name="gender">Female<br><br>

            <label>Nationality</label><br><br>
            <select name="nation">
                <option value="indonesia">Indonesia</option>
                <option value="singapura">Singapura</option>
                <option value="filipina">Filipina</option>
                <option value="vietnam">Vietnam</option>
                <option value="jepang">Jepang</option>
                <option value="korsel">Korea Selatan</option>
            </select><br><br>

            <label>Language Spoken</label><br><br>
            <input type="checkbox" name="bhs.indo">Bahasa Indonesia<br>
            <input type="checkbox" name="bhs.inggris">English<br>
            <input type="checkbox" name="other">Other<br><br>

            <label>Bio</label><br><br>
            <textarea name="bio" rows="10" cols="30"></textarea>
            <br>
            <input type="submit" value="Sign Up">
        </form>
</body>
</html>